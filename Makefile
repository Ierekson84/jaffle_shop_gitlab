manifest:
	pip install pyopenssl --upgrade;
	pip install click --upgrade;
	pip install dbt-core dbt-duckdb dbt-snowflake; 
	dbt parse --project-dir=jaffle_shop --profiles-dir=jaffle_shop/